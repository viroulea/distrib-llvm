(define-module (distrib-llvm)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system trivial)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix utils)
  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages check)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages libffi)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages llvm)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages python)
  #:use-module (gnu packages xml)
  #:use-module (srfi srfi-1)
  #:use-module (ice-9 match)
  #:export (make-lld-wrapper system->llvm-target))

; clang-* patches are actually taken from guix
(define %llvm-patches
  '(("14.0.6" "clang-14.0-libc-search-path.patch")
    ("15.0.7" "clang-15.0-libc-search-path.patch"
     "patches/llvm-15-guix-packaging.patch")
    ("16.0.6" "clang-16.0-libc-search-path.patch"
     "patches/llvm-16-guix-packaging.patch")
    ("17.0.3" "clang-17.0-libc-search-path.patch"
     "patches/llvm-17-guix-packaging.patch")))

; FIXME: duplication from llvm
(define (clang-properties version)
  "Return package properties for Clang VERSION."
  `((compiler-cpu-architectures ("x86_64"
                                 ;; This list was obtained by running:
                                 ;;
                                 ;; guix shell clang -- llc -march=x86-64 -mattr=help
                                 ;;
                                 ;; filtered from uninteresting entries such as "i686" and "pentium".
                                 ,@(if (version>=? version "10.0") ;TODO: refine
                                       '("atom" "barcelona"
                                         "bdver1"
                                         "bdver2"
                                         "bdver3"
                                         "bdver4"
                                         "bonnell"
                                         "broadwell"
                                         "btver1"
                                         "btver2"
                                         "c3"
                                         "c3-2"
                                         "cannonlake"
                                         "cascadelake"
                                         "cooperlake"
                                         "core-avx-i"
                                         "core-avx2"
                                         "core2"
                                         "corei7"
                                         "corei7-avx"
                                         "generic"
                                         "geode"
                                         "goldmont"
                                         "goldmont-plus"
                                         "haswell"
                                         "icelake-client"
                                         "icelake-server"
                                         "ivybridge"
                                         "k8"
                                         "k8-sse3"
                                         "knl"
                                         "knm"
                                         "lakemont"
                                         "nehalem"
                                         "nocona"
                                         "opteron"
                                         "opteron-sse3"
                                         "sandybridge"
                                         "silvermont"
                                         "skx"
                                         "skylake"
                                         "skylake-avx512"
                                         "slm"
                                         "tigerlake"
                                         "tremont"
                                         "westmere"
                                         "x86-64"
                                         "x86-64-v2"
                                         "x86-64-v3"
                                         "x86-64-v4"
                                         "znver1"
                                         "znver2"
                                         "znver3")
                                       '())))))

(define (llvm-uri component version)
  ;; LLVM release candidate file names are formatted 'tool-A.B.C-rcN/tool-A.B.CrcN.src.tar.xz'
  ;; so we specify the version as A.B.C-rcN and delete the hyphen when referencing the file name.
  (string-append "https://github.com/llvm/llvm-project/releases/download"
                 "/llvmorg-"
                 version
                 "/"
                 component
                 "-"
                 (string-delete #\- version)
                 ".src.tar.xz"))

(define* (system->llvm-target #:optional (system (or (and=> (%current-target-system)
                                                      gnu-triplet->nix-system)
                                                     (%current-system))))
  "Return the LLVM target name that corresponds to SYSTEM, a system type such
as \"x86_64-linux\"."
  ;; See the 'lib/Target' directory of LLVM for a list of supported targets.
  (letrec-syntax ((matches (syntax-rules (=>)
                                         ((_ (system-prefix => target) rest
                                             ...)
                                          (if (string-prefix? system-prefix
                                                              system) target
                                              (matches rest ...)))
                                         ((_)
                                          (error
                                           "LLVM target for system is unknown"
                                           system)))))
                 (matches ("aarch64" => "AArch64")
                          ("armhf" => "ARM")
                          ("mips64el" => "Mips")
                          ("powerpc" => "PowerPC")
                          ("riscv" => "RISCV")
                          ("x86_64" => "X86")
                          ("i686" => "X86")
                          ("i586" => "X86"))))

(define %llvm-release-monitoring-url
  "https://github.com/llvm/llvm-project/releases")

(define %llvm-monorepo-hashes
  '(("14.0.6" . "14f8nlvnmdkp9a9a79wv67jbmafvabczhah8rwnqrgd5g3hfxxxx")
    ("15.0.7" . "12sggw15sxq1krh1mfk3c1f07h895jlxbcifpwk3pznh4m1rjfy2")
    ("16.0.6" . "0jxmapg7shwkl88m4mqgfjv4ziqdmnppxhjz6vz51ycp2x4nmjky")
    ("17.0.3" . "1fhrnsv87if7kbqmrsxy2r7ykx3gnr9lmbmvkhvycc91ii4ihybx")))

(define (llvm-monorepo version)
  (origin
    (method git-fetch)
    (uri (git-reference (url "https://github.com/llvm/llvm-project")
                        (commit (string-append "llvmorg-" version))))
    (file-name (git-file-name "llvm-project" version))
    (sha256 (base32 (assoc-ref %llvm-monorepo-hashes version)))
    (patches (map search-patch
                  (assoc-ref %llvm-patches version)))))
; end of duplication

; TODO: document why this doesn't work:
; patch cmake's add_tablegen, so that it does link to the LLVM dylib
; when building not llvm tblgen.
; See discussions here:https://reviews.llvm.org/D30656

(define* (make-llvm-distrib version)
  (package
    (name "llvm")
    (version version)
    (source
     (llvm-monorepo version))
    (build-system cmake-build-system)
    (outputs '("out" "opt-viewer" "static"))
    (arguments
     (list
      #:configure-flags #~(list (string-append "-C "
                                               #$(local-file
                                                  "caches/llvm-release.cmake"))
                                ;; These options are required for cross-compiling LLVM according
                                ;; to <https://llvm.org/docs/HowToCrossCompileLLVM.html>.
                                #$@(if (%current-target-system)
                                       #~((string-append "-DLLVM_TABLEGEN="
                                                         #+(file-append
                                                            this-package
                                                            "/bin/llvm-tblgen"))
                                          #$(string-append
                                             "-DLLVM_DEFAULT_TARGET_TRIPLE="
                                             (%current-target-system))
                                          #$(string-append
                                             "-DLLVM_TARGET_ARCH="
                                             (system->llvm-target))
                                          #$(string-append
                                             "-DLLVM_TARGETS_TO_BUILD="
                                             (system->llvm-target)))
                                       '()))
      ;; Don't use '-g' during the build, to save space.
      #:build-type "Release"
      #:test-target "check-llvm"
      #:phases #~(modify-phases %standard-phases
                   (add-after 'unpack 'change-directory
                     (lambda _
                       (chdir "llvm")))
                   (replace 'install
                     (lambda* (#:key outputs #:allow-other-keys)
                       (invoke "make" "install-guixlibstatic-distribution")
                       (mkdir-p #$output:static)
                       (rename-file (string-append #$output "/lib")
                                    (string-append #$output:static "/lib"))
                       (invoke "make" "install-guixbinlibshared-distribution")))
                   (add-after 'install 'install-opt-viewer
                     (lambda* (#:key outputs #:allow-other-keys)
                       (let* ((opt-viewer-share (string-append #$output:opt-viewer
                                                               "/share")))
                         (mkdir-p opt-viewer-share)
                         (rename-file (string-append #$output
                                                     "/share/opt-viewer")
                                      opt-viewer-share)))))))
    (native-inputs (list python-wrapper perl))
    (inputs (list libffi))
    (propagated-inputs (list zlib)) ;to use output from llvm-config
    (home-page "https://www.llvm.org")
    (synopsis "Optimizing compiler infrastructure")
    (description
     "LLVM is a compiler infrastructure designed for compile-time, link-time,
runtime, and idle-time optimization of programs from arbitrary programming
languages.  It currently supports compilation of C and C++ programs, using
front-ends derived from GCC 4.0.1.  A new front-end for the C family of
languages is in development.  The compiler infrastructure includes mirror sets
of programming tools as well as libraries with equivalent functionality.")
    (license license:asl2.0)
    (properties `((release-monitoring-url unquote %llvm-release-monitoring-url)))))

(define* (clang-runtime-from-llvm llvm
                                  #:optional hash
                                  (patches '()))
  (package
    (name "clang-runtime")
    (version (package-version llvm))
    (source
     (if hash
         (origin
           (method url-fetch)
           (uri (llvm-uri "compiler-rt" version))
           (sha256 (base32 hash))
           (patches (map search-patch patches)))
         (llvm-monorepo (package-version llvm))))
    (build-system cmake-build-system)
    (native-inputs (modify-inputs (package-native-inputs llvm)
                     (prepend gcc-12)))
    (inputs (list llvm libffi))
    (arguments
     `( ;Don't use '-g' during the build to save space.
        #:build-type "Release"
       #:tests? #f ;Tests require gtest
       #:modules ((srfi srfi-1)
                  (ice-9 match)
                  ,@%cmake-build-system-modules)
       #:phases (modify-phases (@ (guix build cmake-build-system)
                                  %standard-phases)
                  ,@(if hash
                        '()
                        '((add-after 'unpack
                                     'change-directory
                                     (lambda _
                                       (chdir "compiler-rt")))))
                  (add-after 'set-paths 'hide-glibc
                    ;; Work around https://issues.guix.info/issue/36882.  We need to
                    ;; remove glibc from CPLUS_INCLUDE_PATH so that the one hardcoded
                    ;; in GCC, at the bottom of GCC include search-path is used.
                    (lambda* (#:key inputs #:allow-other-keys)
                      (let* ((filters '("libc"))
                             (input-directories (filter-map (lambda (input)
                                                              (match input
                                                                ((name . dir) (and
                                                                               (not
                                                                                (member
                                                                                 name
                                                                                 filters))
                                                                               dir))))
                                                            inputs)))
                        (set-path-environment-variable "CPLUS_INCLUDE_PATH"
                                                       '("include")
                                                       input-directories) #t))))))
    (home-page "https://compiler-rt.llvm.org")
    (synopsis "Runtime library for Clang/LLVM")
    (description
     "The \"clang-runtime\" library provides the implementations of run-time
functions for C and C++ programs.  It also provides header files that allow C
and C++ source code to interface with the \"sanitization\" passes of the clang
compiler.  In LLVM this library is called \"compiler-rt\".")
    (license (package-license llvm))
    (properties `((release-monitoring-url unquote %llvm-release-monitoring-url)
                  (upstream-name . "compiler-rt")))

    ;; <https://compiler-rt.llvm.org/> doesn't list MIPS as supported.
    (supported-systems (delete "mips64el-linux" %supported-systems))))

;; This works for llvm 15 onwards
(define* (clang-distrib-from-llvm llvm
                                  clang-runtime
                                  #:optional hash
                                  #:key (patches '())
                                  tools-extra
                                  (properties (append `((release-monitoring-url
                                                         unquote
                                                         %llvm-release-monitoring-url))
                                                      (clang-properties (package-version
                                                                         llvm)))))
  "Produce Clang with dependencies on LLVM and CLANG-RUNTIME, and applying the
given PATCHES. When tools-extra is set, this also packages the tools from
'clang-tools-extra', such as 'clang-tidy', 'pp-trace', 'modularize', and others."
  (package
    (name "clang")
    (version (package-version llvm))
    (source
     (if hash
         (origin
           (method url-fetch)
           (uri (llvm-uri "clang" version))
           (sha256 (base32 hash))
           (patches (map search-patch patches)))
         (llvm-monorepo (package-version llvm))))
    ;; Using cmake allows us to treat llvm as an external library.  There
    ;; doesn't seem to be any way to do this with clang's autotools-based
    ;; build system.
    (build-system cmake-build-system)
    (native-inputs (package-native-inputs llvm))
    (inputs `(("libxml2" ,libxml2)
              ("gcc-lib" ,gcc "lib")
              ,@(package-inputs llvm)))
    (propagated-inputs (list clang-runtime llvm))
    (arguments
     (list
      #:configure-flags #~(list (string-append "-C "
                                               #$(local-file
                                                  ;; FIXME: set version in there!
                                                  ;; call ludo for string-append
                                                  "caches/clang-release.cmake"))
                                ;; Find libgcc_s, crtbegin.o, and crtend.o.
                                (string-append "-DGCC_INSTALL_PREFIX="
                                               (assoc-ref %build-inputs
                                                          "gcc-lib"))
                                #$@(if tools-extra
                                       (list #~(string-append
                                                "-DLLVM_EXTERNAL_CLANG_TOOLS_EXTRA_SOURCE_DIR="
                                                (getcwd)
                                                "/source/clang-tools-extra"))
                                       `())
                                ;; Use a sane default include directory.
                                (string-append "-DC_INCLUDE_DIRS="
                                               (assoc-ref %build-inputs "libc")
                                               "/include")
                                #$@(if (target-riscv64?)
                                       (list "-DLIBOMP_LIBFLAGS=-latomic"
                                        "-DCMAKE_SHARED_LINKER_FLAGS=-latomic")
                                       `()))

      #:make-flags (if (target-riscv64?)
                       #~(list "LDFLAGS=-latomic")
                       #~'())

      ;; Don't use '-g' during the build to save space.
      #:build-type "Release"
      #:tests? #t
      #:test-target "check-clang"
      #:phases `(modify-phases %standard-phases
                  (add-after 'unpack 'add-missing-triplets
                    (lambda _
                      ;; Clang iterates through known triplets to search for
                      ;; GCC's headers, but does not recognize some of the
                      ;; triplets that are used in Guix.
                      (substitute* "lib/Driver/ToolChains/Gnu.cpp"
                        (("\"aarch64-linux-gnu\"," all)
                         (string-append "\"aarch64-unknown-linux-gnu\", " all))
                        (("\"arm-linux-gnueabihf\"," all)
                         (string-append all
                                        " \"arm-unknown-linux-gnueabihf\","))
                        (("\"i686-pc-linux-gnu\"," all)
                         (string-append "\"i686-unknown-linux-gnu\", " all)))
                      #t))
                  (add-after 'unpack 'set-glibc-file-names
                    (lambda* (#:key inputs #:allow-other-keys)
                      (let ((libc (assoc-ref inputs "libc"))
                            (compiler-rt (assoc-ref inputs "clang-runtime"))
                            (gcc (assoc-ref inputs "gcc")))
                        (substitute* "lib/Driver/ToolChain.cpp"
                          (("getDriver\\(\\)\\.ResourceDir")
                           (string-append "\"" compiler-rt "\"")))

                        ;; Make "LibDir" refer to <glibc>/lib so that it
                        ;; uses the right dynamic linker file name.
                        (substitute* "lib/Driver/ToolChains/Linux.cpp"
                          (("(^[[:blank:]]+LibDir = ).*" _ declaration)
                           (string-append declaration "\"" libc "/lib\";\n"))

                          ;; Make clang look for libstdc++ in the right
                          ;; location.
                          (("LibStdCXXIncludePathCandidates\\[\\] = \\{")
                           (string-append
                            "LibStdCXXIncludePathCandidates[] = { \"" gcc
                            "/include/c++\","))

                          ;; Make sure libc's libdir is on the search path, to
                          ;; allow crt1.o & co. to be found.
                          (("@GLIBC_LIBDIR@")
                           (string-append libc "/lib")))
                        #t)))
                  ;; Awkwardly, multiple phases added after the same phase,
                  ;; e.g. unpack, get applied in the reverse order.  In other
                  ;; words, adding 'change-directory last means it occurs
                  ;; first after the unpack phase.
                  (add-after 'unpack 'change-directory
                    (lambda _
                      (chdir "clang")))
                  (add-after 'install 'symlink-cfi_ignorelist
                    (lambda* (#:key inputs outputs #:allow-other-keys)
                      (let* ((out (assoc-ref outputs "out"))
                             (lib-share (string-append out "/lib/clang/"
                                                       ,version "/share"))
                             (compiler-rt (assoc-ref inputs "clang-runtime")) ;FIXME: shouldn't this be a variable?!
                             ;; FIXME: use search-input-file, and provide some
                             ;; kind of way to let it fail if the file doesn't
                             ;; exist?
                             (file-name ,(if (version>=? version "13")
                                             "cfi_ignorelist.txt"
                                             "cfi_blacklist.txt"))
                             ;; FIXME: check if true, it seems to be in share
                             ;; The location varies between Clang versions.
                             (cfi-ignorelist (cond
                                               ((file-exists? (string-append
                                                               compiler-rt "/"
                                                               file-name))
                                                (string-append compiler-rt "/"
                                                               file-name))
                                               (else (string-append
                                                      compiler-rt "/share/"
                                                      file-name)))))
                        (mkdir-p lib-share)
                        ;; Symlink the ignorelist to where Clang expects
                        ;; to find it.
                        ;; Not all architectures support CFI.
                        ;; see: compiler-rt/cmake/config-ix.cmake
                        (when (file-exists? cfi-ignorelist)
                          (symlink cfi-ignorelist
                                   (string-append lib-share "/" file-name))))))
                  (replace 'install
                    (lambda _
                      (invoke "make" "install-distribution")))
                  (add-after 'install 'install-clean-up-/share/clang
                    (lambda* (#:key outputs #:allow-other-keys)
                      (let* ((out (assoc-ref outputs "out"))
                             (compl-dir (string-append out
                                         "/etc/bash_completion.d")))
                        (with-directory-excursion (string-append out
                                                   "/share/clang")
                          (for-each (lambda (file)
                                      (when (file-exists? file)
                                        (delete-file file)))
                                    ;; Delete extensions for proprietary text editors.
                                    '("clang-format-bbedit.applescript"
                                      "clang-format-sublime.py"
                                      ;; Delete Emacs extensions: see their respective Emacs
                                      ;; Guix package instead.
                                      "clang-rename.el" "clang-format.el"))
                          ;; Install bash completion.
                          (when (file-exists? "bash-autocomplete.sh")
                            (mkdir-p compl-dir)
                            (rename-file "bash-autocomplete.sh"
                                         (string-append compl-dir "/clang")))))
                      #t)))))

    ;; Clang supports the same environment variables as GCC.
    (native-search-paths
     (list (search-path-specification
            (variable "C_INCLUDE_PATH")
            (files '("include")))
           (search-path-specification
            (variable "CPLUS_INCLUDE_PATH")
            (files '("include/c++" "include")))
           (search-path-specification
            (variable "OBJC_INCLUDE_PATH")
            (files '("include")))
           (search-path-specification
            (variable "LIBRARY_PATH")
            (files '("lib" "lib64")))))

    (home-page "https://clang.llvm.org")
    (synopsis "C language family frontend for LLVM")
    (description
     "Clang is a compiler front end for the C, C++, Objective-C and
Objective-C++ programming languages.  It uses LLVM as its back end.  The Clang
project includes the Clang front end, the Clang static analyzer, and several
code analysis tools.")
    (properties properties)
    (license (if (version>=? version "9.0") license:asl2.0 ;with LLVM exceptions
                 license:ncsa))))

(define* (libomp-from-llvm llvm clang)
  (package
    (name "libomp")
    (version (package-version llvm))
    (source
     (llvm-monorepo version))
    (build-system cmake-build-system)
    ;; XXX: Note this gets built with GCC because building with Clang itself
    ;; fails (missing <atomic>, even when libcxx is added as an input.)
    (arguments
     (list
      #:configure-flags #~(list "-DLIBOMP_USE_HWLOC=ON"
                                "-DOPENMP_TEST_C_COMPILER=clang"
                                "-DOPENMP_TEST_CXX_COMPILER=clang++")
      #:test-target "check-libomp"
      #:phases #~(modify-phases %standard-phases
                   (add-after 'unpack 'chdir-to-source-and-install-license
                     (lambda _
                       (chdir "openmp")
                       (install-file "LICENSE.TXT"
                                     (string-append #$output "/share/doc")))))))
    (native-inputs (list clang llvm perl pkg-config python))
    (inputs (list `(,hwloc "lib")))
    (home-page "https://openmp.llvm.org")
    (synopsis "OpenMP run-time support library")
    (description
     "This package provides the run-time support library developed
by the LLVM project for the OpenMP multi-theaded programming extension.  This
package notably provides @file{libgomp.so}, which is has a binary interface
compatible with that of libgomp, the GNU Offloading and Multi Processing
Library.")
    (properties `((release-monitoring-url unquote %llvm-release-monitoring-url)
                  (upstream-name . "openmp")))
    (license license:expat)))

(define-public (make-clang-toolchain clang libomp llvm)
  (package
    (name (string-append (package-name clang) "-toolchain"))
    (version (package-version clang))
    (source
     #f)
    (build-system trivial-build-system)
    (arguments
     '(#:modules ((guix build union))
       #:builder (begin
                   (use-modules (ice-9 match)
                                (srfi srfi-26)
                                (guix build union))
                   (let ((out (assoc-ref %outputs "out")))
                     (match %build-inputs
                       (((names . directories) ...)
                        (union-build out directories)))

                     ;; Create 'cc' and 'c++' so that one can use it as a
                     ;; drop-in replacement for the default tool chain and
                     ;; have configure scripts find the compiler.
                     (symlink "clang"
                              (string-append out "/bin/cc"))
                     (symlink "clang++"
                              (string-append out "/bin/c++"))

                     (union-build (assoc-ref %outputs "debug")
                                  (list (assoc-ref %build-inputs "libc-debug")))
                     (union-build (assoc-ref %outputs "static")
                                  (list (assoc-ref %build-inputs "libc-static")))
                     #t))))

    (native-search-paths
     (append (package-native-search-paths clang)
             (list (search-path-specification
                    ;; copied from glibc
                    (variable "GUIX_LOCPATH")
                    (files '("lib/locale"))))))
    (search-paths
     (package-search-paths clang))

    (license (package-license clang))
    (properties (package-properties clang)) ;for 'compiler-cpu-architectures'
    (home-page "https://clang.llvm.org")
    (synopsis "Complete Clang toolchain for C/C++ development")
    (description
     "This package provides a complete Clang toolchain for C/C++
development to be installed in user profiles.  This includes Clang, as well as
libc (headers and binaries, plus debugging symbols in the @code{debug}
output), and Binutils.")
    (outputs '("out" "debug" "static"))
    (inputs `(("clang" ,clang)
              ("ld-wrapper" ,(car (assoc-ref (%final-inputs) "ld-wrapper")))
              ("binutils" ,binutils)
              ("libomp" ,libomp) ;used when linking with '-fopenmp'
              ("libc" ,glibc)
              ("libc-debug" ,glibc "debug")
              ("libc-static" ,glibc "static")))))

(define (llvm-monorepo-with-patches version patches)
  (origin
    (inherit (llvm-monorepo version))
    (patches (append (origin-patches (package-source llvm))
                     (map search-patch patches)))))

;; NOTE: This package was created by someone who is not an active user of MLIR.
;; It was initially created in order for the flang package to exist.
;; Therefore it might not have all the necessary options other usage may
;; require.
;; Unlike LLVM and Clang, there does not seem to be a global shared library in
;; MLIR, and flang's CMake refers to libraries explicitly individually. So this
;; bundles a whole bunch of static libraries.
(define* (mlir-from-llvm llvm
                         #:optional #:key (patches '())
                         (extra_args '()))
  (package
    (name "mlir")
    (version (package-version llvm))
    (source
     (llvm-monorepo-with-patches version patches))
    (build-system cmake-build-system)
    (native-inputs (package-native-inputs llvm))
    (inputs (list llvm))
    (arguments
     (list
      #:build-type "Release"
      #:configure-flags #~(list "-DMLIR_INCLUDE_TESTS=ON"
                                ;; There is no "find lit" section in the
                                ;; CMakeLists.txt like clang does for instance, so
                                ;; tell llvm explicitly about where to find lit.
                                (string-append "-DLLVM_EXTERNAL_LIT="
                                 (getcwd) "/source/llvm/utils/lit/lit.py")
                                ;; These two options let us install mlir-tblgen!
                                ;; NOTE: building a distribution to achieve this should work, but I have
                                ;; not looked into this yet.
                                "-DLLVM_BUILD_UTILS=ON"
                                "-DLLVM_INSTALL_TOOLCHAIN_ONLY=OFF"
                                ;; This disables the installation of object files in
                                ;; lib/objects-Release.
                                "-DMLIR_INSTALL_AGGREGATE_OBJECTS=OFF"
                                #$@extra_args)
      #:test-target "check-mlir"
      #:phases #~(modify-phases %standard-phases
                   (add-after 'unpack 'chdir-to-source
                     (lambda _
                       (chdir "mlir"))))))
    (home-page "https://mlir.llvm.org/")
    (synopsis "Multi-Level Intermediate Representation library")
    (description
     "Novel approach to building reusable and extensible compiler
infrastructure. MLIR aims to address software fragmentation, improve compilation
for heterogeneous hardware, significantly reduce the cost of building domain
specific compilers, and aid in connecting existing compilers together.")
    (license (package-license llvm))))

(define* (flang-from-llvm llvm
                          clang
                          mlir
                          #:optional #:key (patches '())
                          (extra_args '()))
  (package
    (name "flang")
    (version (package-version llvm))
    (source
     (llvm-monorepo-with-patches version patches))
    (native-inputs (modify-inputs (package-native-inputs llvm)
                     (prepend clang mlir)))
    (inputs (list llvm))
    ;; use modify phase to patch:
    ;; patch --batch -i #$(local-file "patches/flang.patch")
    ;; maybe mentions that per-version patch could be used
    (arguments
     (list
      ;; Don't use '-g' during the build to save space.
      #:build-type "Release"
      ;; Yep, for flang we definitely don't want parallel build :(
      ;; FIXME: restore #f; I use build -c 3 (out of 20...)
      #:parallel-build? #t
      #:test-target "check-flang"
      #:configure-flags #~(list (string-append "-DCLANG_DIR="
                                               #$(this-package-native-input
                                                  "clang") "/lib/cmake/clang")
                                ;; There is no "find lit" section in the
                                ;; CMakeLists.txt like clang does for instance, so
                                ;; tell llvm explicitly about where to find lit.
                                (string-append "-DLLVM_EXTERNAL_LIT="
                                 (getcwd) "/source/llvm/utils/lit/lit.py")
                                #$@extra_args)
      #:phases #~(modify-phases %standard-phases
                   (add-after 'unpack 'chdir-to-source
                     (lambda _
                       (chdir "flang"))))))
    (build-system cmake-build-system)
    (home-page "https://flang.llvm.org/")
    (synopsis "Fortran compiler")
    (description
     "Ground-up implementation of a Fortran front end written in
modern C++. While it is capable of generating executables for a number of
examples, some functionalities are still missing.")
    (license (package-license llvm))))

; LLVM 15
(define-public llvm-distrib-15
  (make-llvm-distrib "15.0.7"))

(define-public clang-runtime-distrib-15
  (clang-runtime-from-llvm llvm-distrib-15))

;; FIXME always set clang-tools-extra as an external llvm project.
(define-public clang-distrib-15
  (clang-distrib-from-llvm llvm-distrib-15 clang-runtime-distrib-15
                           #:tools-extra #t))

(define-public libomp-15
  (libomp-from-llvm llvm-distrib-15 clang-distrib-15))

(define-public clang-toolchain-distrib-15
  (make-clang-toolchain clang-distrib-15 libomp-15 llvm-distrib-15))
(define-public mlir-15
  (mlir-from-llvm llvm-distrib-15
                  #:patches (list "patches/mlir-15-external-build.patch")
                  #:extra_args (list "-DLLVM_LIT_ARGS=-sv")))

(define-public flang-15
  (flang-from-llvm llvm-distrib-15
                   clang-distrib-15
                   mlir-15
                   #:patches (list "patches/flang-15-external-build.patch")
                   ;; "Todo" tests fail, but I'm not sure why
                   ;; they are not marked as XFAIL.
                   #:extra_args (list "-DLLVM_LIT_ARGS=-sv --filter-out Todo")))

; LLVM 16
; NOTE to self: llvm@16 is roughly 70MB bigger than 15 mostly because of
; llvm-exegesis
(define-public llvm-distrib-16
  (make-llvm-distrib "16.0.6"))

(define-public clang-runtime-distrib-16
  (clang-runtime-from-llvm llvm-distrib-16))

;; FIXME always set clang-tools-extra as an external llvm project.
(define-public clang-distrib-16
  (clang-distrib-from-llvm llvm-distrib-16 clang-runtime-distrib-16
                           #:tools-extra #t))

(define-public libomp-16
  (libomp-from-llvm llvm-distrib-16 clang-distrib-16))

(define-public clang-toolchain-distrib-16
  (make-clang-toolchain clang-distrib-16 libomp-16 llvm-distrib-16))

(define-public mlir-16
  (mlir-from-llvm llvm-distrib-16
                  #:patches (list "patches/mlir-16-external-build.patch")
                  #:extra_args (list
                                "-DLLVM_LIT_ARGS=-sv --filter-out mlir-pdll-lsp-server")))

(define-public flang-16
  (flang-from-llvm llvm-distrib-16
                   clang-distrib-16
                   mlir-16
                   #:patches (list "patches/flang-16-external-build.patch")
                   #:extra_args (list "-DLLVM_LIT_ARGS=-sv --filter-out Todo")))

; LLVM 17
(define-public llvm-distrib-17
  (make-llvm-distrib "17.0.3"))

(define-public clang-runtime-distrib-17
  (clang-runtime-from-llvm llvm-distrib-17))

;; FIXME always set clang-tools-extra as an external llvm project.
(define-public clang-distrib-17
  (clang-distrib-from-llvm llvm-distrib-17 clang-runtime-distrib-17
                           #:tools-extra #t))

(define-public libomp-17
  (libomp-from-llvm llvm-distrib-17 clang-distrib-17))

(define-public clang-toolchain-distrib-17
  (make-clang-toolchain clang-distrib-17 libomp-17 llvm-distrib-17))

(define-public mlir-17
  (mlir-from-llvm llvm-distrib-17
                  #:patches (list "patches/mlir-17-external-build.patch")
                  #:extra_args (list
                                "-DLLVM_LIT_ARGS=-sv --filter-out mlir-pdll-lsp-server")))

(define-public flang-17
  (flang-from-llvm llvm-distrib-17
                   clang-distrib-17
                   mlir-17
                   #:patches (list "patches/flang-17-external-build.patch")
                   #:extra_args (list "-DLLVM_LIT_ARGS=-sv")))

; Test section with out-of-tree projects

; This is a made up out-of-tree project to test libs from LLVM and Clang.
; It should be simple enough that it builds with any LLVM version.

(define* (example-from-llvm llvm clang flang)
  (package
    (name "llvm-example-distrib")
    (version (package-version llvm))
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://gitlab.inria.fr/viroulea/llvm-example")
             (commit "1.0")))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0c3q33ackh4si3y6nm113j1vh4b7wcf3sprsczqzx1ans1msc5nn"))))
    (build-system cmake-build-system)
    (native-inputs (list flang))
    (inputs (list llvm clang))
    (arguments
     (list
      #:configure-flags #~(list "-DCMAKE_C_COMPILER=clang"
                                "-DCMAKE_CXX_COMPILER=clang++"
                                "-DCMAKE_Fortran_COMPILER=flang-new"
                                (string-append "-DEX_LLVM_VERSION="
                                               #$(package-version llvm))
                                "-DEX_ENABLE_FORTRAN=ON")
      #:build-type "Release"))
    (home-page "https://gitlab.inria.fr/viroulea/hello-cmake")
    (synopsis "Out of tree LLVM project")
    (description
     "This is an example LLVM project to test LLVM/Clang/Flang distribution.")
    (license license:gpl3+)))

(define* (static-example-from-llvm example llvm)
  (package/inherit example
    (name "llvm-example-distrib-static")
    (inputs (modify-inputs (package-inputs example)
              (append `(,llvm "static"))))
    (arguments (substitute-keyword-arguments (package-arguments example)
                 ((#:configure-flags cf)
                  #~(cons "-DEX_BUILD_STATIC=ON"
                          #$cf))))))

(define-public llvm-example-distrib-15
  (example-from-llvm llvm-distrib-15 clang-distrib-15 flang-15))

(define-public llvm-example-distrib-15/static
  (static-example-from-llvm llvm-example-distrib-15 llvm-distrib-15))

(define-public llvm-example-distrib-16
  (example-from-llvm llvm-distrib-16 clang-distrib-16 flang-16))

(define-public llvm-example-distrib-16/static
  (static-example-from-llvm llvm-example-distrib-16 llvm-distrib-16))

(define-public llvm-example-distrib-17
  (example-from-llvm llvm-distrib-17 clang-distrib-17 flang-17))

(define-public llvm-example-distrib-17/static
  (static-example-from-llvm llvm-example-distrib-17 llvm-distrib-17))

; This is a relatively complex out-of-tree project requiring specifically LLVM 15

(define-public parcoach-distrib
  (package
    (name "parcoach-distrib")
    (version "2.4.2")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/parcoach/parcoach")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1sq01gfidly7mkhm233cw8sdrg3jh1xnm238lxf2bcid8vp4issa"))))
    (build-system cmake-build-system)
    (native-inputs (list clang-toolchain-distrib-15 flang-15 python python-lit
                         googletest))
    (inputs (list llvm-distrib-15 openmpi))
    (synopsis "Analysis tool for errors detection in parallel
applications")
    (description "PARCOACH is an Open-source software dedicated to the
collective errors detection in parallel applications.")
    (home-page "https://parcoach.github.io/")
    (arguments
     `(#:build-type "Release"
       #:configure-flags '("-DPARCOACH_ENABLE_FORTRAN=ON"
                           "-DCMAKE_Fortran_COMPILER=flang-new"
                           "-DPARCOACH_VERSION_SUFFIX=''")
       #:test-target "run-lit"
       #:phases (modify-phases %standard-phases
                  (add-before 'check 'mpi-setup
                    ,%openmpi-setup))))
    (license license:lgpl2.1)))

; NOTE: keeping this outdated comment to remember to talk about that in the
; post.
; FIXME: using the static output doesn't work because CMake
; include(ExportStatic) look into the root of module path, but everything is in
; lib/cmake/llvm (because this is where find_package goes).
; Options: with output static, make sure lib/cmake/llvm is appended to the
; CMAKE_MODULE_PATH.
; or set it explicitly, but find out why using this-package-native-input
; "distrib-llvm:static" doesn't work.
; or fix the generation to include part of the path?
(define-public parcoach-distrib/static
  (package/inherit parcoach-distrib
    (name "parcoach-distrib-static")
    (inputs (modify-inputs (package-inputs parcoach-distrib)
              (append `(,llvm-distrib-15 "static"))))
    (arguments (substitute-keyword-arguments (package-arguments
                                              parcoach-distrib)
                 ((#:configure-flags cf)
                  #~(cons "-DPARCOACH_BUILD_SHARED=OFF"
                          #$cf))))))
