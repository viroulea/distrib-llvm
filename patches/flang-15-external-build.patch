From 10ee6b07a3426836c779f51ab98340b6e3416648 Mon Sep 17 00:00:00 2001
From: Philippe Virouleau <philippe.virouleau@inria.fr>
Date: Sat, 2 Dec 2023 14:19:40 +0100
Subject: [PATCH] Patch flang

---
 flang/CMakeLists.txt                  |  7 +++++--
 flang/lib/Frontend/CMakeLists.txt     | 14 +++++++++++---
 flang/lib/FrontendTool/CMakeLists.txt | 13 ++++++++++---
 flang/tools/bbc/CMakeLists.txt        |  4 +++-
 flang/tools/tco/CMakeLists.txt        |  5 ++++-
 llvm/cmake/modules/AddLLVM.cmake      |  1 +
 6 files changed, 34 insertions(+), 10 deletions(-)

diff --git a/flang/CMakeLists.txt b/flang/CMakeLists.txt
index 7e0aec2bf090..f3b2d3dfdc5f 100644
--- a/flang/CMakeLists.txt
+++ b/flang/CMakeLists.txt
@@ -92,8 +92,7 @@ if (FLANG_STANDALONE_BUILD)
   get_filename_component(MLIR_DIR_ABSOLUTE ${MLIR_DIR} REALPATH)
   list(APPEND CMAKE_MODULE_PATH ${MLIR_DIR_ABSOLUTE})
   include(AddMLIR)
-  find_program(MLIR_TABLEGEN_EXE "mlir-tblgen" ${LLVM_TOOLS_BINARY_DIR}
-    NO_DEFAULT_PATH)
+  set(MLIR_TABLEGEN_EXE "${MLIR_INSTALL_PREFIX}/bin/mlir-tblgen")
 
   option(LLVM_INSTALL_TOOLCHAIN_ONLY
     "Only include toolchain files in the 'install' target." OFF)
@@ -146,6 +145,10 @@ if (FLANG_STANDALONE_BUILD)
 #LLVM_BUILD_MAIN_SRC_DIR - Path to llvm source when out-of-tree.
   set(FLANG_GTEST_AVAIL 0)
   if (FLANG_INCLUDE_TESTS)
+    # FIXME: ideally we would do something like clang does here, but to be able to
+    # use an external lit it seems to be enough.
+    find_package(Python3 ${LLVM_MINIMUM_PYTHON_VERSION} REQUIRED
+      COMPONENTS Interpreter)
     set(UNITTEST_DIR ${LLVM_BUILD_MAIN_SRC_DIR}/utils/unittest)
     if(EXISTS ${UNITTEST_DIR}/googletest/include/gtest/gtest.h)
       if (NOT TARGET llvm_gtest)
diff --git a/flang/lib/Frontend/CMakeLists.txt b/flang/lib/Frontend/CMakeLists.txt
index 96769c707f10..94a9f739b91d 100644
--- a/flang/lib/Frontend/CMakeLists.txt
+++ b/flang/lib/Frontend/CMakeLists.txt
@@ -12,10 +12,10 @@ add_flang_library(flangFrontend
   TextDiagnostic.cpp
 
   DEPENDS
-  clangBasic
   FIRBuilder
   FIRDialect
   FIRSupport
+  FIROptCodeGenPassIncGen
   FIROptTransformsPassIncGen
   MLIRIR
   ${dialect_libs}
@@ -26,8 +26,6 @@ add_flang_library(flangFrontend
   FortranEvaluate
   FortranCommon
   FortranLower
-  clangBasic
-  clangDriver
   FIRDialect
   FIRSupport
   FIRBuilder
@@ -48,3 +46,13 @@ add_flang_library(flangFrontend
   FrontendOpenACC
   FrontendOpenMP
 )
+# FIXME: upstream is fixed from LLVM 16 onwards.
+if(CLANG_LINK_CLANG_DYLIB)
+  add_dependencies(flangFrontend clang-cpp)
+else()
+  add_dependencies(flangFrontend clangBasic)
+endif()
+clang_target_link_libraries(flangFrontend PRIVATE
+  clangBasic
+  clangDriver
+)
diff --git a/flang/lib/FrontendTool/CMakeLists.txt b/flang/lib/FrontendTool/CMakeLists.txt
index 0753313d7342..7639910bded8 100644
--- a/flang/lib/FrontendTool/CMakeLists.txt
+++ b/flang/lib/FrontendTool/CMakeLists.txt
@@ -5,15 +5,22 @@ add_flang_library(flangFrontendTool
   # This makes sure that the MLIR dependencies of flangFrontend (which are
   # transitively required here) are generated before this target is build.
   flangFrontend
-  clangBasic
 
   LINK_LIBS
   flangFrontend
-  clangBasic
-  clangDriver
   MLIRPass
 
   LINK_COMPONENTS
   Option
   Support
 )
+# FIXME: upstream is fixed from LLVM 16 onwards.
+if(CLANG_LINK_CLANG_DYLIB)
+  add_dependencies(flangFrontend clang-cpp)
+else()
+  add_dependencies(flangFrontendTool clangBasic)
+endif()
+clang_target_link_libraries(flangFrontendTool PRIVATE
+  clangBasic
+  clangDriver
+)
diff --git a/flang/tools/bbc/CMakeLists.txt b/flang/tools/bbc/CMakeLists.txt
index 5830531dff78..98358a4a8846 100644
--- a/flang/tools/bbc/CMakeLists.txt
+++ b/flang/tools/bbc/CMakeLists.txt
@@ -1,3 +1,6 @@
+set(LLVM_LINK_COMPONENTS
+  Passes
+)
 
 add_flang_tool(bbc bbc.cpp
 DEPENDS
@@ -19,5 +22,4 @@ FortranParser
 FortranEvaluate
 FortranSemantics
 FortranLower
-LLVMPasses
 )
diff --git a/flang/tools/tco/CMakeLists.txt b/flang/tools/tco/CMakeLists.txt
index 329c3864f3fe..94c49ac7bd3e 100644
--- a/flang/tools/tco/CMakeLists.txt
+++ b/flang/tools/tco/CMakeLists.txt
@@ -1,3 +1,7 @@
+set(LLVM_LINK_COMPONENTS
+  Passes
+)
+
 add_flang_tool(tco tco.cpp)
 llvm_update_compile_flags(tco)
 get_property(dialect_libs GLOBAL PROPERTY MLIR_DIALECT_LIBS)
@@ -21,5 +25,4 @@ target_link_libraries(tco PRIVATE
   MLIRParser
   MLIRSupport
   MLIRVectorToLLVM
-  LLVMPasses
 )
diff --git a/llvm/cmake/modules/AddLLVM.cmake b/llvm/cmake/modules/AddLLVM.cmake
index 1f0507f395cf..326e13a8ebf8 100644
--- a/llvm/cmake/modules/AddLLVM.cmake
+++ b/llvm/cmake/modules/AddLLVM.cmake
@@ -2259,6 +2259,7 @@ function(llvm_codesign name)
   endif()
 endfunction()
 
+# FIXME: rpath for clang lib!
 function(llvm_setup_rpath name)
   if(CMAKE_INSTALL_RPATH)
     return()
-- 
2.39.2

