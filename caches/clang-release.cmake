set(CLANG_LINK_CLANG_DYLIB ON CACHE STRING "")
set(CLANG_INCLUDE_TESTS ON CACHE STRING "")
set(LLVM_PARALLEL_LINK_JOBS 1 CACHE STRING "")

# Most of these are expected to fail because of Guix's changes to the driver.
set(FAILING_TESTS "Clang :: ClangScanDeps/resource_directory.c\\\;Clang :: Modules/header-attribs.cpp\\\;Clang :: PCH/reloc.c\\\;Clang :: Headers/cuda_with_openmp.cu")

# Exclude driver-related tests: it's overkill as some of them pass, but it's
# easier than specifying them all. Also we can't mark xfail tests within
# Clang-Unit (or at least I could not figure it out).
set(LLVM_LIT_ARGS "-sv --filter-out \"Driver\" --xfail=\"${FAILING_TESTS}\"" CACHE STRING "")

set(CLANG_TOOLS_BINARIES
  c-index-test
  clang-check
  clang-extdef-mapping
  clang-format
  clang-offload-bundler
  clang-offload-packager
  clang-refactor
  clang-repl
  clang-scan-deps
  diagtool
  find-all-symbols
  hmaptool
  scan-build
  scan-build-py
  scan-view
  # from clang-tools-extra
  clang-apply-replacements
  clang-change-namespace
  clangd
  clang-doc
  clang-include-fixer
  clang-move
  clang-pseudo
  clang-query
  clang-rename
  clang-reorder-fields
  clang-tidy
  modularize
  pp-trace
)

set(LLVM_DISTRIBUTION_COMPONENTS
  clang
  clang-resource-headers
  clang-headers
  libclang
  clang-cpp
  clang-cmake-exports
  # Included here because they were in the Guix LLVM build I've been comparing
  # to. Maybe they are not used?
  clang-tidy-headers
  ${CLANG_TOOLS_BINARIES}
  CACHE STRING "")

# NOTE: the interest of providing static libs is not clear to me.
# When using the CMake helper it will link to clang's dylib since
# CLANG_LINK_CLANG_DYLIB is ON for this distribution.
# The only way to use the static libraries is to use the imported target
# directly, without going through the add_clang_library helper.
# Therefore, for now, I build a single llvm distribution.
