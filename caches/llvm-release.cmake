set(LLVM_BUILD_LLVM_DYLIB ON CACHE STRING "")
set(LLVM_LINK_LLVM_DYLIB ON CACHE STRING "")
set(LLVM_ENABLE_FFI ON CACHE STRING "")
set(LLVM_ENABLE_RTTI ON CACHE STRING "") # for some third-party utilities

# FIXME: with the distribution building system, does this even matter anymore?
set(LLVM_INSTALL_UTILS ON CACHE STRING "") # needed for rustc
set(LLVM_ENABLE_TERMINFO OFF CACHE STRING "")
set(LLVM_PARALLEL_LINK_JOBS 1 CACHE STRING "") # don't kill the builder

# The order here is important, this is an extract from the doc:
# By default, each target can appear in multiple distributions; a target will be installed as part of all distributions it appears in, and it’ll be exported by the last distribution it appears in (the order of distributions is the order they appear in LLVM_DISTRIBUTIONS).
# (currently may mot matter much, did matter in my earlier attempts)
set(LLVM_DISTRIBUTIONS
  GuixBinLibShared
  GuixLibStatic
  CACHE STRING "")

# Avoiding 'LLVM_TOOLCHAINS_TOOL' since it's actually used by llvm.
# This is based on the list from AddLLVM.cmake
set (TOOLCHAIN_TOOLS
  # Aliased to llvm-symbolizer
  llvm-addr2line
  llvm-ar
  llvm-cov
  llvm-cxxfilt
  # Aliased to llvm-ar
  llvm-dlltool
  llvm-dwp
  llvm-ranlib
  # Aliased to llvm-ar
  llvm-lib
  llvm-ml
  llvm-nm
  llvm-objcopy
  # Aliased to llvm-objcopy
  llvm-install-name-tool
  # Aliased to llvm-objdump
  llvm-otool
  llvm-objdump
  llvm-pdbutil
  llvm-rc
  # Aliased to llvm-readobj
  llvm-readelf
  llvm-readobj
  llvm-size
  llvm-strings
  # Aliased to llvm-objcopy
  llvm-strip
  llvm-profdata
  llvm-symbolizer
  )

# ls -al $(guix build llvm@15)/bin
set(OTHER_BINARIES
  bugpoint
  count
  dsymutil
  FileCheck
  llc
  lli
  lli-child-target
  # Aliased to llvm-symbolizer
  llvm-as
  llvm-bcanalyzer
  # Aliased to llvm-objcopy
  llvm-bitcode-strip
  llvm-cat
  llvm-cfi-verify
  llvm-config
  llvm-c-test
  llvm-cvtres
  llvm-cxxdump
  llvm-cxxmap
  llvm-debuginfod
  llvm-debuginfod-find
  llvm-diff
  llvm-dis
  llvm-dwarfdump
  llvm-dwarfutil
  llvm-exegesis
  llvm-extract
  llvm-gsymutil
  llvm-ifs
  llvm-jitlink
  llvm-jitlink-executor
  llvm-libtool-darwin
  llvm-link
  llvm-lipo
  #llvm-lit
  llvm-lto
  llvm-lto2
  llvm-mc
  llvm-mca
  llvm-modextract
  llvm-mt
  llvm-opt-report
  llvm-PerfectShuffle
  llvm-profgen
  llvm-reduce
  llvm-remark-size-diff
  llvm-rtdyld
  llvm-sim
  llvm-split
  llvm-stress
  llvm-tapi-diff
  llvm-tblgen
  llvm-tli-checker
  llvm-undname
  # Aliased to llvm-rc
  llvm-windres
  llvm-xray
  not
  obj2yaml
  opt
  sancov
  sanstats
  split-file
  UnicodeNameMappingGenerator
  verify-uselistorder
  yaml2obj
  yaml-bench
  # FIXME: Moved to its own output by Guix; why not put it with the main output?
  opt-viewer
  CACHE STRING "")

# NOTE: I did attempt to package toolchain binaries and other utils/tools
# separately, but llvm-config is very confused about this since it assumes
# everything lies next to it.
# Therefore I just package all binaries together; the only thing "broken" now
# is finding static libs through llvm-config.
# NOTE: I just switched to ToolchainOnly: I'd like an output with the toolchain
# binaries necessary to create clang-toolchain.
#set(LLVM_ToolchainOnly_DISTRIBUTION_COMPONENTS
  #${TOOLCHAIN_TOOLS}
  #CACHE STRING "")

set(LLVM_GuixBinLibShared_DISTRIBUTION_COMPONENTS
  LLVM
  LTO
  Remarks
  # NOTE: these 3 are actually used by some of the tools in clang.
  # Maybe put the static output in the "build-only" section of clang?
  # NOTEbis: I patched add_tablegen so that tablegens others than llvm-tblgen
  # depend on the dylib. llvm-tblgen cannot depend on the dylib because of a
  # dependency cycle.
  # See discussions here:https://reviews.llvm.org/D30656
  LLVMSupport
  LLVMTableGen
  LLVMDemangle
  llvm-headers
  cmake-exports
  guixbinlibshared-cmake-exports
  ${TOOLCHAIN_TOOLS}
  ${OTHER_BINARIES}
  CACHE STRING "")

set(LLVM_GuixLibStatic_DISTRIBUTION_COMPONENTS
  # NOTE: since I do not include LLVMSupport/TableGen/Demangle here, they will
  # not considered as added here, and won't be exported because they have already
  # been exported. It's weird but at the moment it's fine, because in Guix anything
  # depending on devstatic will also depend on devshared (basically use the llvm-full).
  llvm-libraries
  guixlibstatic-cmake-exports
  CACHE STRING "")
